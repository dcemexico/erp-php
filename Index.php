<?php
session_start();
if(!empty($_SESSION['authenticated'])) header('Location: modules/home/home.php'); ?>
<!DOCTYPE>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/login.css">
</head>
<body>
	<div class="main">
  	<div class="container">
			<center>
    		<div id="login">
      		<form method="POST" action="return false" onsubmit="return false">
        		<fieldset class="clearfix">
          		<p><span class="fa fa-user"></span><input type="text" name="username" id="username" Placeholder="Username" required></p>
          		<p><span class="fa fa-lock"></span><input type="password" name="password" id="password" Placeholder="Password" required></p>
           		<div>
                <a class="forgot" href="#">Forgot password?</a>
                <span class="input-submit">
                	<input type="submit" value="Sign In" onclick="signIn();">
                </span>
            	</div>
        		</fieldset>
						<div class="clearfix"></div>
						<div class="errors" id="errors"></div>
      		</form>
      		<div class="clearfix"></div>
    		</div>
	    	<div class="logo"> ERP
	      	<div class="clearfix"></div>
	    	</div>
			</center>
		</div>
	</div>

 	<script type="text/javascript" src="vendor/jquery/dist/jquery.min.js"></script>
 	<script type="text/javascript" src="assets/js/login.js"></script>

</body>
</html>
