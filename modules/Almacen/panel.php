<!DOCTYPE>
<html>
<head>
	<title>Whare House</title>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../../vendor/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../../vendor/fixedColumns.dataTables.css">
	<link rel="stylesheet" href="../../assets/css/panel.css">
	<link rel="stylesheet" href="../../assets/css/tema_tabla.css">
	<!-- <link rel="stylesheet" href="../../assets/css/item.css"> -->
</head>
<body>

	<?php
		include "../header_menu.php";
		include "../../database/conexion.php";
	?>

	<div class="container">
		<h2>Whare House</h2>
		<hr />
		<!--<div class="row">
			<div class="col-md-2">
				<a id="show_new_item" class="btn btn-success" href="ScannEntrada.php" onClick="javascript:window.open('','Entrada','width=1350,height=670,scrollbars=no')" target="Entrada"> <i class="fa fa-plus"></i> Entrada</a>
			</div>
			<div class="col-md-2">
				<a id="show_new_item" class="btn btn-success" href="ScannSalida.php" onClick="javascript:window.open('','Salida','width=1350,height=670,scrollbars=no')" target="Salida"> <i class="fa fa-plus"></i> Salida</a>
			</div>
		</div>
<hr />-->
		
		<?php
		$Width_no = "40px";
		$Width_mat = "100px";
		$Width_desc = "250px";
		$Width_mtype = "150px";
		$Width_unit = "60px";
		$Width_loc = "150px";
		$Width_status = "150px";
		$Width_date = "150px";
		?>
		<div class="contenedor">
			<div  class="jqxgrid" id="dvData">
	      <table id="example" class="" >
					<thead>
						<tr>
							<th width="<?=$Width_no?>">#</th>
							<th width="<?=$Width_mat?>">Material</th>
							<th width="<?=$Width_desc?>">Descripcion</th>
							<th width="<?=$Width_mtype?>">Stock</th>
							<th width="<?=$Width_unit?>">Locacion</th>
						</tr>
					</thead>
					<tbody>
				<?php
					$fecha_actual = date("Y-m-d");
					$query = "SELECT * FROM almacenstock ";
					$result = $mysqli -> query($query);
					$cont = 1;

					while($reg = $result -> fetch_array()) {
						$material_search = $reg['material'];
						$descripcion_search = $reg['descripcion'];
						$stock_search = $reg['stock'];
						$locacion_search = $reg['locacion'];
						?>
						<tr>
							<td><?=$cont?></td>
							<td><?=$material_search?></td>
							<td><?=$descripcion_search?></td>
							<td><?=$stock_search?></td>
							<td><?=$locacion_search?></td>
						</tr>
						<?php
						$cont++;
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

<script type="text/javascript" src="../../vendor/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="../../vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../vendor/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="../../vendor/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../vendor/dataTables.jqueryui.js"></script>
<script type="text/javascript" src="../../vendor/dataTables.fixedColumns.js"></script>

<script type="text/javascript" language="javascript" src="../../assets/js/item.js"></script>
</body>
</html>
