<!DOCTYPE html>
<html>
<head>
	<title>test</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

	<link rel="stylesheet" href="../../vendor/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../../vendor/fixedColumns.dataTables.css">
	<link rel="stylesheet" href="../../assets/css/panel.css">
	<link rel="stylesheet" href="../../assets/css/tema_tabla.css">
</head>
<body>
<?php include "../../database/conexion.php"; ?>
<table id="table1" width="500" >
					<thead>
						<tr>
							<th width="50">#</th>
							<th width="110">Material</th>
							<th width="100">Cantidad</th>
							<th width="100">Linea</th>
							<th width="110">Fecha</th>
						</tr>
					</thead>
					<tbody>
				<?php
					$fecha_actual = date("Y-m-d");
					$query = "SELECT * FROM producplan order by secuencia ";
					$result = $mysqli -> query($query);
					$cont = 1;

					while($reg = $result -> fetch_array()) {
						$id = $reg['id'];
						$materia_Plan = $reg['material'];
						$cantidad_Plan = $reg['cantidad'];
						$linea_plan = $reg['linea'];
						$fecha_plan = $reg['fecha'];

						$item = "item-".$id;
						?>
						<tr id="<?=$item?>">
							<td ><?=$cont?></td>
							<td ><?=$materia_Plan?></td>
							<td ><?=$cantidad_Plan?></td>
							<td ><?=$linea_plan?></td>
							<td ><?=$fecha_plan?></td>
						</tr>
						<?php
						$cont++;
					}
					?>
					</tbody>
				</table>
<script type="text/javascript">
	$(document).ready(function(){
    $( "tbody" ).sortable();
});


	$( "tbody" ).sortable({
    // Cancel the drag when selecting contenteditable items, buttons, or input boxes
    cancel: ":input,button,[contenteditable]",
    // Set it so rows can only be moved vertically
    axis: "y",
    // Triggered when the user has finished moving a row
    update: function (event, ui) {
        // sortable() - Creates an array of the elements based on the element's id. 
        // The element id must be a word separated by a hyphen, underscore, or equal sign. For example, <tr id='item-1'>
        var data = $(this).sortable('serialize');
        
        //alert(data); <- Uncomment this to see what data will be sent to the server
 
        // AJAX POST to server
        $.ajax({
            data: data,
            type: 'POST',
            url: 'testsavesort.php',
            success: function(response) {
	        // alert(response); //<- Uncomment this to see the server's response
	    }
        });
    }
});
</script>

<!--<script type="text/javascript" src="../../vendor/jquery/dist/jquery.min.js"></script>-->
<script type="text/javascript" src="../../vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<!--<script type="text/javascript" src="../../vendor/jquery-1.12.3.min.js"></script>-->
<script type="text/javascript" src="../../vendor/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../vendor/dataTables.jqueryui.js"></script>
<script type="text/javascript" src="../../vendor/dataTables.fixedColumns.js"></script>

<script type="text/javascript" language="javascript" src="../../assets/js/item.js"></script>				
</body>
</html>