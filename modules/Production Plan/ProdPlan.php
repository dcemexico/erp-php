<!DOCTYPE>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../../vendor/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../../vendor/fixedColumns.dataTables.css">
	<link rel="stylesheet" href="../../assets/css/panel.css">
	<link rel="stylesheet" href="../../assets/css/tema_tabla.css">
	<!-- <link rel="stylesheet" href="../../assets/css/item.css"> -->
</head>
<body>

	<?php
		include "../header_menu.php";
		include "../../database/conexion.php";
	?>

	<div class="container">
		<h2>Production Plan</h2>
		<hr />
		<div class="row">
			<div class="col-md-2">
				<a href="../Production Plan/CreatePlan.php" onClick="javascript:window.open('','new','width=300,height=470,scrollbars=no')" target="new" class="btn btn-success"> <i class="fa fa-plus"></i> Create Plan</a>
			</div>
		</div>
<hr />
		

		

		<?php
		$Width_no = "40px";
		$Width_mat = "100px";
		$Width_desc = "250px";
		$Width_mtype = "150px";
		$Width_unit = "60px";
		$Width_loc = "150px";
		$Width_status = "150px";
		$Width_date = "150px";
		?>
		<div class="contenedor">
			<div  class="jqxgrid" id="dvData">
	      <table id="table1" width="500" >
					<thead>
						<tr>
							<th width="50">#</th>
							<th width="170">Material</th>
							<th width="100">Cantidad</th>
							<th width="100">Linea</th>
							<th width="110">Fecha</th>
						</tr>
					</thead>
					<tbody>
				<?php
					$fecha_actual = date("Y-m-d");
					$query = "SELECT * FROM producplan where fecha = '$fecha_actual' order by secuencia ";
					$result = $mysqli -> query($query);
					$cont = 1;

					while($reg = $result -> fetch_array()) {
						$id = $reg['id'];
						$materia_Plan = $reg['material'];
						$cantidad_Plan = $reg['cantidad'];
						$linea_plan = $reg['linea'];
						$fecha_plan = $reg['fecha'];

						$item = "item-".$id;
						?>
						<tr id="<?=$item?>">
							<td width="50"><?=$cont?></td>
							<td width="110"><?=$materia_Plan?></td>
							<td width="100"><?=$cantidad_Plan?></td>
							<td width="100"><?=$linea_plan?></td>
							<td width="110"><?=$fecha_plan?></td>
						</tr>
						<?php
						$cont++;
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<!--<script type="text/javascript" src="../../vendor/jquery/dist/jquery.min.js"></script>-->
<script type="text/javascript" src="../../vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<!--<script type="text/javascript" src="../../vendor/jquery-1.12.3.min.js"></script>-->
<script type="text/javascript" src="../../vendor/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../vendor/dataTables.jqueryui.js"></script>
<script type="text/javascript" src="../../vendor/dataTables.fixedColumns.js"></script>

<script type="text/javascript" language="javascript" src="../../assets/js/item.js"></script>
</body>
</html>
