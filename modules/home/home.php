<!DOCTYPE>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../../vendor/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../../assets/css/panel.css">
</head>
<body>

	<?php include "../header_menu.php"; ?>

	<script type="text/javascript" src="../../vendor/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="../../vendor/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
