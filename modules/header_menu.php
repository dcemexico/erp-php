<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Project name</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">

        <li class="active"><a href="../home/home.php">Home</a></li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ingenering <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="../items/item.php">Item</a></li>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Production <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="../Produccion/panel.php">Label & Scann</a></li>
            <li><a href="../Production Plan/ProdPlan.php">Production Plan</a></li>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Whare House <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="../Almacen/ScannEntrada.php" onClick="javascript:window.open('','Entrada','width=1350,height=670,scrollbars=no')" target="Entrada">incoming</a></li>
            <li><a href="../Almacen/ScannSalida.php" onClick="javascript:window.open('','Salida','width=1350,height=670,scrollbars=no')" target="Salida">outcoming</a></li>
            <li><a href="../Almacen/panel.php">Stock</a></li>
          </ul>
        </li>

        <li><a href='../../modules/login/logout.php'>Log out</a></li>

      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>
