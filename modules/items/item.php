<!DOCTYPE>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../../vendor/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../../vendor/fixedColumns.dataTables.css">
	<link rel="stylesheet" href="../../assets/css/panel.css">
	<link rel="stylesheet" href="../../assets/css/tema_tabla.css">
	<!-- <link rel="stylesheet" href="../../assets/css/item.css"> -->
</head>
<body>

	<?php
		include "../header_menu.php";
		include "../../database/conexion.php";
	?>

	<div class="container">
		<h2>Items</h2>
		<hr />
		<div class="row">
			<div class="col-md-2">
				<a id="show_new_item" class="btn btn-success"> <i class="fa fa-plus"></i> New material</a>
			</div>
			<div class="col-md-4">
				<a id="show_edit_item" class="btn btn-warning"> <i class="fa fa-edit"></i> Edit material</a>
			</div>
			<div class="col-md-4">
				<a id="show_new_line" class="btn btn-success"> <i class="fa fa-plus"></i> New Line</a>
			</div>
		</div>
<hr />
		<form id="content_new_item" class="hide_content well" action="guardar.php" method="post">
			<div class="row">
				<div class="col-md-6">
					<label>Material:</label>
					<input type="text" name="materialf" class="form-control" required>
				</div>
				<div class="col-md-6">
					<label>Descripcion:</label><br>
					<textarea name="descripcion" class="form-control"></textarea>
				</div>
				<div class="col-md-6">
					<label>Tipo de material:</label>
					<select name="MatTypes" class="form-control" required>
						<option value="">--</option>
						<option value="Finish Good">Finish Good</option>
						<option value="Raw Material">Raw Material</option>
					</select>
				</div>
				<div class="col-md-6">
					<label>unidad:</label>
					<select class="form-control" name="unidad">
						<option value="">--</option>
						<option value="M">M</option>
						<option value="Pz">Pz</option>
						<option value="Kg">Kg</option>
						<option value="L">L</option>
					</select>
				</div>
				<div class="col-md-6">
					<label>Locación</label>
					<select class="form-control" name="Locación">
						<option value="">--</option>
						<option value="Rack A">Rack A</option>
						<option value="Rack B">Rack B</option>
						<option value="Rack C">Rack C</option>
						<option value="Rack D">Rack D</option>
					</select>
				</div>
				<div class="col-md-6">
					<br />
					<button type="submit" class="btn btn-success">Guardar</button>
					<button type="reset" class="btn btn-danger">Eliminar</button>
				</div>
			</div>
		</form>

		<iframe src="EditIframe.php" id="content_edit_item" class="hide_content well" width="101%" height="425"></iframe>

<form id="content_new_Line" class="hide_content well" action="createLine.php" method="post">
			<div class="row">
				<div class="col-md-6">
					<label>Material:</label>
					<input type="text" name="materialC" class="form-control" required>
				</div>
				<div class="col-md-6">
					<label>Line:</label>
					<input type="text" name="createLine" class="form-control" required>
				</div>
				<div class="col-md-6">
				<br>
					<button type="submit" class="btn btn-success">Guardar</button>
					<button type="reset" class="btn btn-danger">Eliminar</button>
				</div>
			</div>
		</form>

		<?php
		$Width_no = "40px";
		$Width_mat = "100px";
		$Width_desc = "250px";
		$Width_mtype = "150px";
		$Width_unit = "60px";
		$Width_loc = "150px";
		$Width_status = "150px";
		$Width_date = "150px";
		?>
		<div class="contenedor">
			<div  class="jqxgrid" id="dvData">
	      <table id="example" class="" >
					<thead>
						<tr>
							<th width="<?=$Width_no?>">#</th>
							<th width="<?=$Width_mat?>">Material</th>
							<th width="<?=$Width_desc?>">Descripcion</th>
							<th width="<?=$Width_mtype?>">Material Type</th>
							<th width="<?=$Width_unit?>">Unidad</th>
							<th width="<?=$Width_loc?>">Locacion</th>
							<th width="<?=$Width_status?>">Status</th>
							<th width="<?=$Width_date?>">fecha captura</th>
						</tr>
					</thead>
					<tbody>
				<?php
					$fecha_actual = date("Y-m-d");
					$query = "SELECT * FROM item where  item_status = 'Activo' ";
					$result = $mysqli -> query($query);
					$cont = 1;

					while($reg = $result -> fetch_array()) {
						$material_serach = $reg['item_material'];
						$descripcion_serach = $reg['item_descripcion'];
						$mattype_serach = $reg['item_matType'];
						$unit_serach = $reg['item_unit'];
						$location_serach = $reg['item_location'];
						$status_serach = $reg['item_status'];
						$fecha_serach = $reg['item_fecha'];
						?>
						<tr>
							<td><?=$cont?></td>
							<td><?=$material_serach?></td>
							<td><?=$descripcion_serach?></td>
							<td><?=$mattype_serach?></td>
							<td><?=$unit_serach?></td>
							<td><?=$location_serach?></td>
							<td><?=$status_serach?></td>
							<td><?=$fecha_serach?></td>
						</tr>
						<?php
						$cont++;
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

<script type="text/javascript" src="../../vendor/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="../../vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../vendor/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="../../vendor/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../vendor/dataTables.jqueryui.js"></script>
<script type="text/javascript" src="../../vendor/dataTables.fixedColumns.js"></script>

<script type="text/javascript" language="javascript" src="../../assets/js/item.js"></script>
</body>
</html>
