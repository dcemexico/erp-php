<!DOCTYPE html>
<html>
<head>
	<title>Item Panel</title>
<link rel="stylesheet" type="text/css" href="css/item_style.css">
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="css/temaTabla.css">
<link rel="stylesheet" type="text/css" href="css/fixedColumns.dataTables.css">	

<script type="text/javascript" language="javascript" src="js/jquery-1.12.3.min.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="js/dataTables.jqueryui.js"></script>
<script type="text/javascript" language="javascript" src="js/dataTables.fixedColumns.js"></script>
<script type="text/javascript" src="js/dropdown.js"></script>
</head>
<body>
<?php 
include "../../menu/menu_bar.php"; 
include "../../../database/conexion.php"; 
?>

<div class="cap_bar">
	<button class="newItem" id="btn_newItem">New Item</button>
	<div id="dropdown_div_newItem">
		<form action="guardar.php" method="post">
			<label>Material:</label>
			<input type="text" name="materialf" class="material" required><br>
			<label>Descripcion:</label><br>
			<textarea name="descripcion"></textarea><br>
			<label>Tipo de material:</label>
			<select name="MatTypes" required>
				<option value="">--</option>
				<option value="Finish Good">Finish Good</option>
				<option value="Raw Material">Raw Material</option>
			</select><br>
			<label>unidad:</label>
			<select name="unidad">
				<option value="">--</option>
				<option value="M">M</option>
				<option value="Pz">Pz</option>
				<option value="Kg">Kg</option>
				<option value="L">L</option>
			</select><br>
			<label>Locación</label>
			<select name="Locación">
				<option value="">--</option>
				<option value="Rack A">Rack A</option>
				<option value="Rack B">Rack B</option>
				<option value="Rack C">Rack C</option>
				<option value="Rack D">Rack D</option>
			</select><br>
			<button type="submit" class="guardar">Guardar</button>
			<button type="reset" class="eliminar">Eliminar</button>
		</form>
	</div>

	<button class="editItem" id="btn_editItem">Edit Item</button>
	<div id="dropdown_div_editItem">
		<form action="guardar.php" method="post">
			<label>seach:</label>
			<select>
				<option value="">--</option>
				<?php 
              $consulta = "SELECT item_material FROM item where item_material != ' ' group by item_material ";
		        $resultado = $mysqli -> query($consulta);
		        while($fila = $resultado -> fetch_array())
		        {
		         $codeMaterial = $fila["item_material"];
		         echo "<option value='$codeMaterial'>$codeMaterial</option>";
		        }
              ?>
			</select>
		</form>
	</div>

</div>
<?php 
$Width_no = "40px";
$Width_mat = "100px";
$Width_desc = "250px";
$Width_mtype = "150px";
$Width_unit = "60px";
$Width_loc = "150px";
$Width_status = "150px";
$Width_date = "150px";
?>
<div class="contenedor">
	<div  class="jqxgrid" id="dvData">
  
      <table id="example" class="Display nowrap" >
		<thead>
			<tr>	
				<th width="<?=$Width_no?>">#</th>
				<th width="<?=$Width_mat?>">Material</th>
				<th width="<?=$Width_desc?>">Descripcion</th>
				<th width="<?=$Width_mtype?>">Material Type</th>
				<th width="<?=$Width_unit?>">Unidad</th>
				<th width="<?=$Width_loc?>">Locacion</th>
				<th width="<?=$Width_status?>">Status</th>
				<th width="<?=$Width_date?>">fecha captura</th>
			</tr>
		</thead>
		<tbody>
		<?php 
$fecha_actual = date("Y-m-d");		
$query = "SELECT * FROM item where item_fecha = '$fecha_actual' and item_status = 'Activo' ";
$result = $mysqli -> query($query);
$cont = 1;
while($reg = $result -> fetch_array())
{
	$material_serach = $reg['item_material'];
	$descripcion_serach = $reg['item_descripcion'];
	$mattype_serach = $reg['item_matType'];
	$unit_serach = $reg['item_unit'];
	$location_serach = $reg['item_location'];
	$status_serach = $reg['item_status'];
	$fecha_serach = $reg['item_fecha'];

		?>
			<tr>
				<td><?=$cont?></td>
				<td><?=$material_serach?></td>
				<td><?=$descripcion_serach?></td>
				<td><?=$mattype_serach?></td>
				<td><?=$unit_serach?></td>
				<td><?=$location_serach?></td>
				<td><?=$status_serach?></td>
				<td><?=$fecha_serach?></td>
			</tr>
	<?php 
	$cont++;
	}
	?>		
		</tbody>
		</table>
		</div>
</div>
<script type="text/javascript" language="javascript" class="init">
  
$(document).ready(function() {
  var table = $('#example').DataTable( {
    scrollY:        "300px",
    scrollX:        true,
    scrollCollapse: false,
    sort: false,
    paging:         false,
    fixedColumns:   {
      leftColumns: 0,
      rightColumns: 0
    }

  } );

} );

  </script>
</div>
</body>
</html>