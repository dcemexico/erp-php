<!DOCTYPE>
<html>
<head>
  <title></title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../../vendor/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../assets/css/panel.css">
  <style type="text/css">
  	body{
  		margin: 0;
  		padding: 0;
  	}
  </style>
</head>
<body>
<?php 
include "../../database/conexion.php";

$Search_code = $_GET['searchCode'];
$query = "SELECT * FROM item where  item_material = '$Search_code' ";
	$result = $mysqli -> query($query);	
	
	while($reg = $result -> fetch_array()) {
		$id_search = $reg['item_id'];
		$material_serach = $reg['item_material'];
		$descripcion_serach = $reg['item_descripcion'];
		$mattype_serach = $reg['item_matType'];
		$unit_serach = $reg['item_unit'];
		$location_serach = $reg['item_location'];
		$status_serach = $reg['item_status'];
	}
	if ($material_serach === $Search_code) {
		$Data = "Codigo: ".$material_serach; 
	}else{
		$Data = "Registro (".$Search_code.") no encontrado en base de datos ";
	}
	
?>
<form method="get">
  <input type="text" name="searchCode" class="form-control" required placeholder="Buscar Codigo"><br>
  <button type="submit" class="btn btn-info">Buscar</button><i style="margin-left: 15px;"><?=$Data?></i>
</form>
  <form  action="editar.php" method="post">
			<div class="row">
				<div class="col-md-6">
					<label>Material:</label>
					<input type="text" name="materialf" class="form-control" value="<?=$material_serach?>" required>
					<input type="text" name="id_Search"  value="<?=$id_search?>" readonly hidden required>
				</div>
				<div class="col-md-6">
					<label>Descripcion:</label><br>
					<textarea name="descripcion" class="form-control"><?=$descripcion_serach?></textarea>
				</div>
				<div class="col-md-6">
					<label>Tipo de material:</label>
					<select name="MatTypes" class="form-control" required>
						<option value="<?=$mattype_serach?>"><?=$mattype_serach?></option>
						<option value="">--</option>
						<option value="Finish Good">Finish Good</option>
						<option value="Raw Material">Raw Material</option>
					</select>
				</div>
				<div class="col-md-6">
					<label>unidad:</label>
					<select class="form-control" name="unidad">
						<option value="<?=$unit_serach?>"><?=$unit_serach?></option>
						<option value="">--</option>
						<option value="M">M</option>
						<option value="Pz">Pz</option>
						<option value="Kg">Kg</option>
						<option value="L">L</option>
					</select>
				</div>
				<div class="col-md-6">
					<label>Locación</label>
					<select class="form-control" name="Locación">
						<option value="<?=$location_serach?>"><?=$location_serach?></option>
						<option value="">--</option>
						<option value="Rack A">Rack A</option>
						<option value="Rack B">Rack B</option>
						<option value="Rack C">Rack C</option>
						<option value="Rack D">Rack D</option>
					</select>
				</div>
        <div class="col-md-6">
					<label>Status</label>
					<select class="form-control" name="status">
						<option value="<?=$status_serach?>"><?=$status_serach?></option>
						<option value="">--</option>
						<option value="Activo">Activo</option>
						<option value="Inactivo">Inactivo</option>
					</select>
				</div>
				<div class="col-md-6">
					<br />
					<button type="submit" class="btn btn-success">Editar</button>
				</div>
			</div>
		</form>
</body>
</html>
