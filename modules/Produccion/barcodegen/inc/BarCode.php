<?php

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.

require("Graphics.php");

/* Main Abstract Class */

class BarCode
{

########### BARCODE CLASS VARIABLES

/* Graphic Container where de barcode will be drawn */
var $g;

/* type of barcode. */
var $barType;

/* text to be painted as barcode. */
var $code;

/** if true (default), the checksum character will be calculated and appended to the code.
 */
var $checkCharacter;

// if true (default), it will be also shown in the text
var $showCheckChar; 

/** text after encoding. It will contain the appenden checksum, if calculated.
 */
var $codeText;

/* where to put the text*/
var $textOnTop;

/* Bar measures */
var $narrowBarPixels;
var $wideBarPixels;
var $barHeight;

/** top margin of the barcode (also know as quite zone). 
 */
var $topMargin;
var $leftMargin;

/** font & size of the text.
 */
var $textFont;
var $textSize;

##### COLORS

// Description and color identifier (used by graphic funcions)
// for background, bars and text (font), 

var $bgColorDesc;
var $bgColorId;
var $barColorDesc;
var $barColorId;
var $fontColorDesc;
var $fontColorID;
var $fontSize;


/** size in pixels of modules (narrow bars or spaces). 
 */
var $X; // X-dimension

/** multiplicator value for wide bars. A value of 2 (default) means 
that wide bars will be 2*X pixels of width.
 */
var $N;

/** current position of painting cursor.
 */
var $currentX;
var $currentY;

/*** CHARACTER SET RELATED VARIABLES
*
* Tables for each bacode type. They are initialized inside de paintXXX() 
*  function the first time the function is called.
***/

// only digits barcodes use just this
var $digitset; 

// Other barcodes may use the following
var $base_set; 
var $extended_set;
var $set_keys;
var $set_indexes;

#### to be initialized in specific functions

var $patternColor;
var $startChar;
var $stopChar;

// Some codes add one bar to the end of each character. In such case
// it will be assingned in the init() function. (value "n" or "w").
// Default is empty string so no separator bar is appended.
var $separatorBar;

// flag to detect first call to paint()
var $firstPaint;

// flag to tell paintChar() to start with a "white" bar
var $whiteFirst;

// Stores invalid character in paintLoop() and acts also as a flag;
var $charNotInSet;

var $errorDesc;

// Sum of character indexes that maybe used for check char calculation
var $sum;
var $doSum; // calculate $sum

########## CONSTRUCTOR (INITIALIZATION TO DEFAULT VALUES)

function BarCode() {

   $this->g = new Graphics;
   $this->codeText="";

   $this->textOnTop=FALSE;
   $this->setCheckCharacter(TRUE);
   $this->showCheckChar = TRUE;

   $this->setTopMargin( 10 );
   $this->setLeftMargin( 10 );

   $this->textFont = "DEFAULT-3";

   $this->setX( 1 );
   $this->setN( 2.0 );
   $this->setBarHeight( 50 );

   $this->currentX=0;
   $this->currentY=0;

   $this->separatorBar = "";

   $this->patternColor = "bwbwbwbwbwbwbwbwbwbw";

   $this->firstPaint = TRUE;
   $this->whiteFirst = FALSE;

   $this->charNotInSet = '';
   $this->errorDesc = '';

   $this->sum = 0;
   $this->doSum = TRUE;

   $this->setBGColor("WHITE");
   $this->setBarColor("BLACK");
   $this->setFontColor("BLACK");
   $this->fontSize = 10;
}

#################### INITIALIZATION METHODS

function setCheckCharacter( $scc ) {
   $this->checkCharacter = $scc;
}
	
function setX( $x ) {
   if ( $x >= 1 ) {
      $this->X = (int) $x;
      $this->firstPaint = TRUE;
   }
}

function setN( $n ) {
   if ( $n >= 1 ) {
      $this->N = (float) $n;
      $this->firstPaint = TRUE;
   }
}

function setQuiteZone( $m ) {
   $this->topMargin  = (int) $m;
   $this->leftMargin = (int) $m;
}

function setTopMargin( $m ) {
   $this->topMargin  = (int) $m;
}

function setLeftMargin( $m ) {
   $this->leftMargin = (int) $m;
}

function setBarHeight( $bh ) {
   if ( $bh >= 1 ) 
      $this->barHeight = (int) $bh;
}

function setBGColor( $c ) {
  $this->bgColorDesc = $c;
}

function setBarColor( $c ) {
  $this->barColorDesc = $c;
}

function setFontColor( $c ) {
  $this->fontColorDesc = $c;
}

function setTextFont( $f ) {
   $this->textFont = $f;
}

function setFontSize( $s ) {
   $this->fontSize = $s;
}

function setRotation( $r ) {
  $this->g->setRotation( $r );
}

function setImageType( $t, $q=0 ) {

   $this->g->setType($t,$q);
}

function setFilePath( $fp ) {

   $this->g->setFilePath( $fp );
}

// Can be extended by child classes that may want to do something 
// one time before proceeding with paint() calls.
// if any of the parameters used here changes, the corresponding setParam()
// function should initialize $this->firstPaint

function init() {

   $this->narrowBarPixels = $this->X;
   $this->wideBarPixels = $this->X * $this->N;
}

#################### PAINT (MAIN METHOD)

function paint( $code, $filename='' ) {

   // Some initializations

   $this->code = $code;

   if ( $this->firstPaint ) {
     $this->firstPaint = FALSE;
     $this->init();
   }

   if ( isset($this->base_set) ) {
     // Access to keys by numerical index
     // In some case is already set by child class constructor
     if ( empty( $this->set_keys ) ) 
       $this->set_keys = array_keys( $this->base_set );
     // Access to numerical index by key (ASCII character)
     $this->set_indexes = array_flip( $this->set_keys );
   }

   $this->charNotInSet = '';

   $this->paintInit();

   // Call PaintBody()

   if ( $this->canPaint() ) {
  
      // Paint barcode & draw text (or error)
   
      if ( $this->paintBody() ) { 
         $this->drawText();
      } else { 
         $this->drawError();
      }
   }

   // Reduce image size
   // We assume simetric right/bottom margins

   $this->g->reduceSize( 
      $this->currentX + $this->leftMargin,
      $this->currentY + $this->topMargin);  

   $this->paintFinish();

   // Output Image (to browser or file) -returns file name-
  
   return $this->g->outputImage( $code, $filename );
}


#################### PAINT INITILIALIZATIONS

/** 

Calculates the following instance values:

$this->wideBarPixels
*/

function paintInit() {

   /*** create and initial graphic container big enough
   
   Its width will depend on the number of bars and 
   Assuming N > 2 (wide bar at least doubles narrow bar),
   MSI is the longest bar type with ( N * 4 ) + 4 minimum length.
   We increase it to 5*n +5 just in case. We also 5 add chars to consider
   start/stop/guard/check chars.

   For the height, we assume the text won't be bigger than the bars.
   ***/

   $w = (int) ( $this->leftMargin * 2 +
      ( $this->narrowBarPixels * (5*$this->N+5) * (strlen($this->code)+5) ) );
   $h = (int) ( $this->topMargin * 2 + $this->barHeight * 2.5 );

   $this->g->create($w,$h);

   $this->bgColorId   = $this->g->useColor( $this->bgColorDesc ); 
   $this->barColorId  = $this->g->useColor( $this->barColorDesc ); 
   $this->fontColorId = $this->g->useColor( $this->fontColorDesc ); 
   $this->g->setFont($this->textFont, $this->fontSize );

   $this->g->setColor( $this->barColorId );

   $this->currentX = $this->leftMargin;

   $this->sum = 0;

   #$this->codeText .= "_".$this->narrowBarPixels;
   #$this->codeText .= "-".$this->wideBarPixels;
   #$this->codeText .= "_";
}

########## PAINT FINISH

/*** Does nothing at this moment. May be used by child classes to perform
any initialization needed for a subseq�ent paint() call that wouldn't work
if put in paintInit() ***/


function paintFinish() {
}

##########  CANPAINT (VALIDATIONS)

function canPaint() {

   // Detect possible rotation error

   if ( ! $this->g->canRotateFont() ) {
      $this->errorDesc = "Rotation error: use True Type Font";
      $this->g->setRotation(0);
      $this->g->setFont("DEFAULT-2");
      $this->DrawError();
      return FALSE;
   }

   // Verify all characters in code are valid
   // and obtain sumatory for simple types

   $this->preProcess();

   if ( $this->charNotInSet OR $this->errorDesc ) {
      $this->drawError();
      return FALSE;
   }

   // If we reach this point, all validations are done.

   return TRUE;
}

##########  PRE-PROCESS

function preProcess() {

   // SIMPLE LOOP FOR ONLY DIGITS SET

   if ( isset($this->digitset) ) {

      for ( $i = 0; $i < strlen($this->code); $i++ ) {
        $c = $this->code{$i};
        if ( $c < '0' or $c > '9' ) {
            $this->charNotInSet = $c;
            return;
        }
        $this->sum += $c;
      }
      return;
   }


   if ( isset($this->base_set) ) {

      if ( ! isset( $this->extended_set ) ) {

         // Loop for basic bar types

         for ( $i = 0; $i < strlen($this->code); $i++ ) {
            $c = $this->code{$i};
            if ( arrayKeyExists( $c, $this->base_set ) ) {
               $this->sum += $this->set_indexes[$c];
            } else {
               $this->charNotInSet = $c;
               return;
            }
         }

      } else {

         // Loop for extended bar types
     
         for ( $i = 0; $i < strlen($this->code); $i++ ) {
            $c = $this->code{$i};
            $b = ord( $c );
            if ( $b <= 128 ) {
               $encoded = $this->extended_set[ $b ];
               if ( $this->doSum ) for ( $j=0; $j < strlen($encoded); $j++ ) 
                  $this->sum += 5* $this->set_indexes[ $encoded{$j} ];
            } else {
               $this->charNotInSet = $c;
               return;
            }
         }  
      }  

      return;
   }


   // We should never reach this point.
}

########### PAINT LOOP

function paintLoop() {


   // SIMPLE LOOP FOR ONLY DIGITS SET

   if ( isset($this->digitset) ) {

      for ( $i = 0; $i < strlen($this->code); $i++ )
        $this->paintChar( $this->digitset[ $this->code{$i} ] );
      return;
   }

   if ( isset($this->base_set) ) {

      if ( ! isset( $this->extended_set ) ) {

         // Loop for basic bar types
 
         for ( $i = 0; $i < strlen($this->code); $i++ )
            $this->paintChar( $this->base_set[ $this->code{$i} ] );

      } else {
  
         // Loop for extended bar type (CODE39EXT, CODE93EXT)
  
         for ( $i = 0; $i < strlen($this->code); $i++ ) 
            // NOTE: this function must be defined in child class
            $this->paintExtendedChar($this->extended_set[ ord($this->code{$i}) ]);
      }

      return;
   }

   // We should never reach this point.
}


#################### PAINT BODY (MAIN CODE)

function paintBody() {

// text to show -- default: same code to paint
// specific classes may modify it, i.e. to add check char.
$this->codeText = $this->code;

// Add check char. to code now, if appropriate

if ( $this->checkCharacter ) {
   $this->addCheckChar();
   if ( $this->showCheckChar ) $this->codeText = $this->code;
}

########## DRAW BARS

// paint start character
// paint chars in string -paintLoop()-
//  (May set $this->charNotInSet to indicate error.)
// paint stop character

$this->paintChar( $this->startChar );
$this->paintLoop();
if ( $this->charNotInSet ) return FALSE;
$this->paintChar( $this->stopChar );
return TRUE;

}

####################  ADD CHECK CHAR

// default: nothing, it always should be defined in child classes

function addCheckChar() {}


#################### DRAW TEXT

function drawText() {

   if ( $this->textFont === NULL ) {
      $this->currentY = $this->barHeight + $this->topMargin;
      return;
   }
   
   // Calculate X pixel

   $TextH = $this->g->getFontHeight();
   $endOfCode = $this->currentX;

   $toCenterX = (int) ( $endOfCode - $this->leftMargin 
      - $this->g->stringWidth( $this->codeText ) ) / 2 ;

   if ( $toCenterX < 0 ) $toCenterX = 0;

   $x = $this->leftMargin + $toCenterX;

   // Y pixel

   $this->currentY = $this->barHeight + $TextH + 1 + $this->topMargin;

   if ( $this->textOnTop )
    $y = $TextH + 4;
   else
    $y = $this->currentY;

   // draw it

   $this->g->setColor( $this->fontColorId );
   $this->g->drawString( $this->codeText, $x, $y );
}

#################### DRAW ERROR

function drawError() { 
  
   $h = $this->g->getFontHeight();
   $x = $this->leftMargin;
   $y = $this->topMargin + $h;

   $msg = 'ERROR drawing "'.$this->code.'"';
   $w = $this->g->stringWidth($msg);
   $this->drawErrorString( $msg, $x, $y, $w, $h ); 

   if ( $this->charNotInSet )
      $msg = 'Invalid character "'.$this->charNotInSet.'"';
   else 
      { if ( $this->errorDesc ) $msg = $this->errorDesc; }

   $y += $h * 1.5;
   $w = $this->g->stringWidth($s);
   $this->drawErrorString( $msg, $x, $y, $w, $h); 

   $this->currentY = $y + $h;
}

function drawErrorString( $s, $x, $y, $w, $h ) {

   $this->g->setColor( $this->bgColorId );
   $this->g->fillRect( $x-1, $y, $w, $h );
   $this->g->setColor( $this->fontColorId );
   $this->g->drawString( $s, $x, $y ); 
   if ( $x + $w > $this->currentX )
      $this->currentX = $x + $w;
}
      


########## CHECK CHARACTER CALCULATION

// Used by CODE11 and CODE93

function weightedCheckChar( $w, $sum, $limit, $div, $dir="DOWN" ) {

   if ( $dir == "UP" ) {
     for ( $i = 0; $i <= strlen($this->code)-1; $i++) {
       $c = $this->code{$i};
       if ( arrayKeyExists( $c, $this->base_set ) ) {
         $sum += $this->set_indexes[$c] * $w++; 
         if ( $w == $limit ) $w = 1;
       }
     }
   } else {
     for ( $i = strlen($this->code)-1; $i>=0; $i--) {
       $c = $this->code{$i};
       if ( arrayKeyExists( $c, $this->base_set ) ) {
         $sum += $this->set_indexes[$c] * $w++; 
         if ( $w == $limit ) $w = 1;
       }
     }
   }

   return $sum % $div;
}


##########   ODD3-MOD10 CHECK CHARACTER 

// Used by  EAN*, UPC* & *25 codes 

function odd3mod10CheckChar() {

   if ( ! $this->checkCharacter ) return;

   $len = strlen($this->code);

   $odd = TRUE;
   $sumodd = 0;
   $sum = 0;

   for ( $i = $len-1; $i >= 0; $i-- ) {
     if ($odd) $sumodd += $this->code{$i};
     else      $sum    += $this->code{$i};
     $odd = (!$odd);
   }

   $sum += $sumodd * 3;
   $c = $sum % 10;

   if ( $c != 0) $c = 10 - $c;

   $this->code .= $c;
}


########## PAINTS A CHARACTER 

function paintChar( $patternBars )  {

   $patternBars .= $this->separatorBar;
   $cBar="";

   #for ( $i=0; $i < strlen($this->patternColor); $i++) {

   if ( $this->whiteFirst )
     $ic = 1;
   else
     $ic = 0;
   
   for ( $i=0; $i < strlen($patternBars); $i++) {
   
      $black = ( $this->patternColor{$ic++} == 'b' );
      $cBar = $patternBars{$i};
   
      switch( $cBar ) {
      case 'n': 
         $this->addBar( $this->narrowBarPixels, $black );
         break;
      case 'w': 
         $this->addBar( $this->wideBarPixels, $black );
            break;
      case '1': 
      case '2': 
      case '3': 
         case '4': 
         $this->addBar(  (int)( $this->narrowBarPixels * $cBar ),
             $black );
         break;
      }
   
   } // of for

} 


#################### ADD BAR

// adds a bar to the bar code at the currentX position.

function addBar($w,$black) {

   if ($black) {
      $this->g->fillRect( $this->currentX, $this->topMargin,
         $w, $this->barHeight );
   }

   // move pointer
   $this->currentX += $w;

}


} // of class BarCode

?>