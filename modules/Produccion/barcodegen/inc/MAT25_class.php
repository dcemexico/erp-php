<?php

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.


require("SET25.php");

class MAT25 extends SET25 {

######### CONSTRUCTOR

function MAT25() {

   parent::SET25();

   // MAT25 uses SET25 with an additional narrow bar at
   // the end of each character.

   for ( $i = 0; $i <= 9; $i++ )
      $this->digitset[$i] .= "n";

   $this->startChar = "wnnnnn";
   $this->stopChar  = "wnnnnn";
}

########## ADD CHECK CHAR

// No check char. Ignore SET25 default.

function addCheckChar() {}

} // OF CLASS

?>