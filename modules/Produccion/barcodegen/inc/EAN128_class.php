<?php

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.


require("CODE128_class.php");

class EAN128 extends CODE128 {

var $ai; // Application Identifiers

######### CONSTRUCTOR

function EAN128() {

   parent::CODE128();
   $this->ean128 = TRUE;
}

########## PRE PROCESS PHASE 2 ( PARSE EAN128 FIELDS )

/* 
   - Obtain Human Readable interpretation ($this->codeText)
   - detect possible errors 
   - recreate $this->code with only needed separators
*/

function preProcess_2() {

   $this->len = strlen($this->code);

   $this->codeText = '';
   $newCode = '';
   $i = 0;

   while ( $i < $this->len ) {

      // Take two first digits from current position 

      $d2 = substr( $this->code, $i, 2 );

      // Get Application Identifier length

      $aiLen = $this->getAiLen( $d2 );

      // Error AI length not defined

      if ( $aiLen == 0 ) {
         #$this->errorDesc = "App.Id. $d2 not supported";
         $this->codeText = $this->code;
         return;
      }

      // Get Fixed Field Length

      $fixedLen = $this->getFixedFieldLen( (int) $d2 );

      // Get Field Length (space used as delimiter)

      $j = $i;
      while ( $j < $this->len AND $this->code{$j} != SPACE ) $j++;
      $fieldLen = $j - $i;

      // Verify fixed field length matches

      if ( $fixedLen > 0 AND $fieldLen != $fixedLen ) {
         #$this->errorDesc = "AI $d2: Invalid field length";
         $this->codeText = $this->code;
         return;
      } 

      $dataLen = $fieldLen - $aiLen;

      // add field to newCode
      $newCode .= substr( $this->code, $i, $fieldLen );

      // add field to codeText -put AI between ()-

      if ( $this->codeText ) $this->codeText .= ' ';
      $this->codeText .= '('.substr( $this->code, $i, $aiLen ).')';
      $this->codeText .= substr( $this->code, $i+$aiLen, $dataLen );

      // increment index & eventually skip delimiter
      $i += $fieldLen;
      if ( $this->code{$i} == SPACE ) $i++;

      // eventually add separator to new code
      if ( $fixedLen == 0 AND $i < $this->len ) $newCode .= SPACE;

   }

   $this->code = $newCode;
   $this->len = strlen($this->code);
}


##### GET Fixed Field Length (AI + data)

/* 0 means variable length.
  Variable length fields have indeed a maximum length.
  However this is not supported.
*/
   
function getFixedFieldLen( $i ) {

   if ( $i == 0 )
      return 20;
   elseif ( $i <= 3 )
      return 16;
   elseif ( $i == 4 )
      return 18;
   elseif ( $i <= 10 )
      return 0;
   elseif ( $i <= 19 )
      return 8;
   elseif ( $i == 20 )
      return 4;
   elseif ( $i <= 30 )
      return 0;
   elseif ( $i <= 36 )
      return 10;
   elseif ( $i == 41 )
      return 16;
   else
      return 0;
}

##### GET APPLICATION IDENTIFIER LENGTH

// 0 means non-defined A.I.
// we check only two digits. 

function getAiLen( $d2 ) {

   switch ( $d2{0} ) {

   case '0':
     if ( $d2 >= '00' AND $d2 <= '04' ) return 2;
     else return 0;

   case '1':
      return 2;

   case '2':
     if ( $d2 >= '20' AND $d2 <= '22' ) return 2;
     elseif ( $d2 >= '23' AND $d2 <= '25' ) return 3;
     else return 0;

   case '3':
      if ( $d2 == '38' ) return 0;
      else if ( $d2 == '30' OR $d2 == '37' ) return 2;
      else return 4;

   case '4':
     if ( $d2 >= '40' AND $d2 <= '42' ) return 3;
     else return 0;

   case '7': 
      if ( $d2 == '70' ) return 4;
      else return 0;

   case '8': 
      if ( $d2 == '80' OR $d2 == '81' ) return 4;
      else return 0;

   case '9': 
      return 2;

   default: 
      return 0;
   }
}

########## PRE PROCESS PHASE 

function preProcess_3() {

   $this->preProcess_3a();
   $this->encoded .= FNC1;
   $this->preProcess_3b();

   // MAIN LOOP

   while ( $this->i < $this->len ) $this->oneDataLoop();

   $this->preProcess_3c();
}


} // OF CLASS

?>