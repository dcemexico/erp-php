<?php

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.

require("BarCode.php");

class MSI extends BarCode {

######### CONSTRUCTOR

function MSI() {

   parent::BarCode();

   if ( empty($this->digitset) ) 
      $this->digitset = array (
         'nwnwnwnw','nwnwnwwn','nwnwwnnw','nwnwwnwn','nwwnnwnw',
         'nwwnnwwn','nwwnwnnw','nwwnwnwn','wnnwnwnw','wnnwnwwn' );

   $this->startChar = "wn";
   $this->stopChar  = "nwn";
}

###### ADD CHECK CHARACTER

function addCheckChar() { 

   $len = strlen($this->code);

   $odd = TRUE;
   $sumodd = "";
   $sum = 0;

   for ( $i = $len-1; $i >= 0; $i-- ) {
     if ($odd) $sumodd .= $this->code{$i};
     else      $sum    += $this->code{$i};
     $odd = (!$odd);
   }

   $sumodd = $sumodd * 2;
   $sumodd = "".$sumodd;

   for ( $i = strlen($sumodd)-1; $i >= 0; $i-- )
      $sum += $sumodd{$i};

   $c = $sum % 10;

   if ( $c != 0) $c = 10 - $c;

   $this->code .= $c;
}


} // OF CLASS

?>