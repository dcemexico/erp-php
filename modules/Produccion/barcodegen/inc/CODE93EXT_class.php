<?php 

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.


require("CODE93_class.php");

class CODE93EXT extends CODE93 {

########## CONSTRUCTOR

function CODE93EXT() {

   parent::CODE93();

   $this->barType = "CODE93EXT";
   $this->doSum = FALSE;

   if ( empty($this->extended_set) ) 
     $this->extended_set = array ( 
   '_2U','_1A','_1B','_1C','_1D','_1E','_1F','_1G',
   '_1H','_1I','_1J','_1K','_1L','_1M','_1N','_1O',
   '_1P','_1Q','_1R','_1S','_1T','_1U','_1V','_1W',
   '_1X','_1Y','_1Z','_2A','_2B','_2C','_2D','_2E',
   ' '  ,'_3A','_3B','_3C','_3D','_3E','_3F','_3G',
   '_3H','_3I','_3J','_3K','_3L',  '-',  '.','_3O',
   '0'  ,'1'  , '2' ,'3'  ,'4'  ,'5'  ,'6'  ,'7'  ,
   '8'  ,'9'  ,'_3Z','_2F','_2G','_2H','_2I','_2J',
   '_2V','A'  ,'B'  ,'C'  ,'D'  ,'E'  ,'F'  ,'G'  ,
   'H'  ,'I'  ,'J'  ,'K'  ,'L'  ,'M'  ,'N'  ,'O'  ,
   'P'  ,'Q'  ,'R'  ,'S'  ,'T'  ,'U'  ,'V'  ,'W'  ,
   'X'  ,'Y'  ,'Z'  ,'_2K','_2L','_2M','_2N','_2O',
   '_2W','_4A','_4B','_4C','_4D','_4E','_4F','_4G',
   '_4H','_4I','_4J','_4K','_4L','_4M','_4N','_4O',
   '_4P','_4Q','_4R','_4S','_4T','_4U','_4V','_4W',
   '_4X','_4Y','_4Z','_2P','_2Q','_2R','_2S','_2T' );

}

########## ADD CHECK CHARACTER


function paintLoop() {

   Barcode::paintLoop();

   $this->codeText = $this->code;
   $cod1 = $this->code93ext_checkchar( 1, 0, 21 );
   $cod2 = $this->code93ext_checkchar( 2, $cod1, 16 );
   $ch1 = $this->set_keys[$cod1];
   $ch2 = $this->set_keys[$cod2];
   $this->paintChar( $this->base_set[ $ch1 ] );
   $this->paintChar( $this->base_set[ $ch2 ] );
   $this->codeText .= $ch1{0};
   $this->codeText .= $ch2{0};
}



function code93ext_checkchar( $w, $sum, $limit ) {

   for ( $i = strlen($this->code)-1; $i>=0; $i--) {

      $b = ord( $this->code{$i} );

      if ( $b <= 128 ) {
         $encoded = $this->extended_set[ $b ];

         if ( strlen($encoded) == 3 ) {
            // control char, _1,_2,_3, or _4
            $c = "".$encoded{0}.$encoded{1};
            if ( arrayKeyExists( $c, $this->base_set ) ) {
               $pos = $this->set_indexes[$c];
               $sum += $pos * ($w+1);
            }
            $c = "".$encoded{2};
            if ( arrayKeyExists( $c, $this->base_set ) ) {
               $pos = $this->set_indexes[$c];
               $sum += $pos * $w;
            }
            $w++;
            if ( $w == $limit ) $w = 1;
            $w++;
            if ( $w == $limit ) $w = 1;
         } else {
            $c = "".$encoded{0};
            if ( arrayKeyExists( $c, $this->base_set ) ) {
               $pos = $this->set_indexes[$c];
               $sum += $pos * $w;
            }
            $w++;
            if ( $w == $limit ) $w = 1;
         }

      } // if $b

   }  // for

   return $sum % 47;
}

########## PAINT EXTENDED CHARACTER

function paintExtendedChar( &$encoded ) {

   if ( strlen($encoded) == 3 ) {

      // paint control char, _1,_2,_3, or _4

      $c = "".$encoded{0}.$encoded{1};
      $this->paintChar( $this->base_set[$c] );
      $c = "".$encoded{2};
   }
   else 
      $c = "".$encoded{0};

   // paint data char

   $this->paintChar( $this->base_set[$c] );
         
}


} // of class CODE93EXT

?>