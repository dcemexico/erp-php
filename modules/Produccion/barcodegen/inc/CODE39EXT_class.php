<?php 

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.

require("CODE39_class.php");

class CODE39EXT extends CODE39 {

########### CONSTRUCTOR

function CODE39EXT() {

   parent::CODE39();

   $this->barType = "CODE39EXT";

   if ( empty($this->extended_set) ) 
     $this->extended_set = array ( 
     '%U','$A','$B','$C','$D','$E','$F','$G',
     '$H','$I','$J','$K','$L','$M','$N','$O',
     '$P','$Q','$R','$S','$T','$U','$V','$W',
     '$X','$Y','$Z','%A','%B','%C','%D','%E',
     ' ' ,'/A','/B','/C','/D','/E','/F','/G',
     '/H','/I','/J','/K','/L', '-', '.','/O',
     '0' ,'1' ,'2' ,'3' ,'4' ,'5' ,'6' ,'7' ,
     '8' ,'9' ,'/Z','%F','%G','%H','%I','%J',
     '%V','A' ,'B' ,'C' ,'D' ,'E' ,'F' ,'G' ,
     'H' ,'I' ,'J' ,'K' ,'L' ,'M' ,'N' ,'O' ,
     'P' ,'Q' ,'R' ,'S' ,'T' ,'U' ,'V' ,'W' ,
     'X' ,'Y' ,'Z' ,'%K','%L','%M','%N','%O',
     '%W','+A','+B','+C','+D','+E','+F','+G',
     '+H','+I','+J','+K','+L','+M','+N','+O',
     '+P','+Q','+R','+S','+T','+U','+V','+W',
     '+X','+Y','+Z','%P','%Q','%R','%S','%T' );

}


########### PAIN EXTENDED CHARACTER

function paintExtendedChar( $encoded ) {

   for ( $j=0; $j < strlen($encoded); $j++ ) 
      $this->paintChar( $this->base_set[ $encoded{$j} ] );
}

} // of class CODE39EXT

?>