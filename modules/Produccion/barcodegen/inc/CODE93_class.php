<?php 

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.

require("BarCode.php");

class CODE93 extends Barcode {

########## CONSTRUCTOR

function CODE93() {

   parent::BarCode();

   $this->barType = "CODE93";

   if ( empty($this->base_set) ) 
   $this->base_set = array (
   '0'=>'131112', '1'=>'111213', '2'=>'111312', '3'=>'111411', 
   '4'=>'121113', '5'=>'121212', '6'=>'121311', '7'=>'111114',
   '8'=>'131211', '9'=>'141111', 'A'=>'211113', 'B'=>'211212',
   'C'=>'211311', 'D'=>'221112', 'E'=>'221211', 'F'=>'231111',
   'G'=>'112113', 'H'=>'112212', 'I'=>'112311', 'J'=>'122112',
   'K'=>'132111', 'L'=>'111123', 'M'=>'111222', 'N'=>'111321',
   'O'=>'121122', 'P'=>'131121', 'Q'=>'212112', 'R'=>'212211',
   'S'=>'211122', 'T'=>'211221', 'U'=>'221121', 'V'=>'222111',
   'W'=>'112122', 'X'=>'112221', 'Y'=>'112121', 'Z'=>'123111',
   '-'=>'121131', '.'=>'311112', ' '=>'311211', '$'=>'321111',
   '/'=>'112131', '+'=>'113121', '%'=>'211131', '_1'=>'121211',
   '_2'=>'312111', '_3'=>'311121', '_4'=>'122211' );

   $this->startChar = "111141";
   $this->stopChar  = "1111411";
   $this->showCheckChar = FALSE;
}

########## ADD CHECK CHARACTER

function paintLoop() {

   parent::paintLoop();

   $this->codeText = $this->code;
   $cod1 = $this->weightedCheckChar( 1, 0, 21, 47 );
   $cod2 = $this->weightedCheckChar( 2, $cod1, 16, 47);
   $ch1 = $this->set_keys[$cod1];
   $ch2 = $this->set_keys[$cod2];
   $this->paintChar( $this->base_set[ $ch1 ] );
   $this->paintChar( $this->base_set[ $ch2 ] );
   $this->codeText .= $ch1{0};
   $this->codeText .= $ch2{0};
}

}

?>