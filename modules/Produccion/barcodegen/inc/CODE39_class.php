<?php 

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.

require("BarCode.php");

class CODE39 extends BarCode {

// space between 2 characters. Multiplicator of X. Default 1.
var $I;
var $separator; // same in pixels

########## CONSTRUCTOR

function CODE39() {

   parent::BarCode();

   $this->barType = "CODE39";

   if ( empty($this->base_set) ) 
   $this->base_set = array (
   '0'=>'nnnwwnwnn', '1'=>'wnnwnnnnw', '2'=>'nnwwnnnnw',
   '3'=>'wnwwnnnnn', '4'=>'nnnwwnnnw', '5'=>'wnnwwnnnn',
   '6'=>'nnwwwnnnn', '7'=>'nnnwnnwnw', '8'=>'wnnwnnwnn',
   '9'=>'nnwwnnwnn', 'A'=>'wnnnnwnnw', 'B'=>'nnwnnwnnw',
   'C'=>'wnwnnwnnn', 'D'=>'nnnnwwnnw', 'E'=>'wnnnwwnnn',
   'F'=>'nnwnwwnnn', 'G'=>'nnnnnwwnw', 'H'=>'wnnnnwwnn',
   'I'=>'nnwnnwwnn', 'J'=>'nnnnwwwnn', 'K'=>'wnnnnnnww',
   'L'=>'nnwnnnnww', 'M'=>'wnwnnnnwn', 'N'=>'nnnnwnnww',
   'O'=>'wnnnwnnwn', 'P'=>'nnwnwnnwn', 'Q'=>'nnnnnnwww',
   'R'=>'wnnnnnwwn', 'S'=>'nnwnnnwwn', 'T'=>'nnnnwnwwn',
   'U'=>'wwnnnnnnw', 'V'=>'nwwnnnnnw', 'W'=>'wwwnnnnnn',
   'X'=>'nwnnwnnnw', 'Y'=>'wwnnwnnnn', 'Z'=>'nwwnwnnnn',
   '-'=>'nwnnnnwnw', '.'=>'wwnnnnwnn', ' '=>'nwwnnnwnn',
   '$'=>'nwnwnwnnn', '/'=>'nwnwnnnwn', '+'=>'nwnnnwnwn',
   '%'=>'nnnwnwnwn', '*'=>'nwnnwnwnn');

   $this->startChar = $this->base_set["*"];
   $this->stopChar  = $this->base_set["*"];
}

########## PARAMETERS

// calculate interchar separator lenght (in pixels)

function setI( $i ) {
   if ( $i <= 0 ) $i = 1; 
   $this->I = $i;
   $this->separator = (int) ( $i * $this->X );
   if ( $this->separator == 0 ) $this->separator=1;
}

########## INIT

function init() {

   parent::init();
   // Default interchar separator if not set
   if ( empty($this->I) ) $this->setI(1);
}


########## ADD CHECK CHARACTER

function addCheckChar() {
   $this->code .= $this->set_keys[ (int) $this->sum % 43 ];
}

########## PAINT CHARACTER (EXTENSION)

function paintChar( $patternBars ) {

   parent::paintChar( $patternBars );
   $this->currentX += $this->separator;
}

} // of class CODE39

?>