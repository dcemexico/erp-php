<?php

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.

require("EAN.php");

class EAN13 extends EAN {

######### CONSTRUCTOR

function EAN13() {

   parent::EAN();
   $this->codeLength = 13;
}

#################### PAINT LOOP

function paintLoop() {

   $sets = $this->setEANCode[ $this->code{0} ];
   $this->whiteFirst = TRUE;

   // second flag char
   $this->paintChar( $this->setA[ $this->code{1} ] );

   // paint left group of chars 

   for ( $i = 2; $i <= 6 ; $i++ ) {
      $c = $this->code{$i};
      if ( $sets{$i-2} == 'B') 
         $this->paintChar( $this->setB[$c] );
      else
         $this->paintChar( $this->setA[$c] );
   }

   // paint center guard Pattern ( $i == 6 )

   $this->leftTextEnd = $this->currentX;
   $this->paintGuardChar("nnnnn");
   $this->rightTextStart = $this->currentX;

   // right group of chars

   $this->whiteFirst = FALSE;

   for ( $i = 7; $i <= 12 ; $i++ ) {
      $c = $this->code{$i};
      $this->paintChar( $this->setA[$c] );
   }

}

#################### DRAW TEXT

function drawText() {

  $this->drawSplitText( 
      $this->codeText{0}, 
      substr($this->codeText,1,6), 
      substr($this->codeText,7,6),
      "" );
}

} // OF CLASS

?>