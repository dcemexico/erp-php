<?php

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.

require("BarCode.php");

define ( 'FNC1',   chr(102) );
define ( 'CODE_A', chr(101) );
define ( 'CODE_B', chr(100) );
define ( 'CODE_C', chr(99) );
define ( 'SHIFT',  chr(98) );
define ( 'START_A',chr(103) );
define ( 'START_B',chr(104) );
define ( 'START_C',chr(105) );
define ( 'STOP',   chr(106) );

// We use the space as EAN128 field delimiter, as it isn't valid char.
// For CODE128, it's valid, and encoded as any other

define ( 'SPACE',  ' ' );


class CODE128 extends BarCode {

var $bars_set; 
var $set; // current set
var $encoded; // encoded bar code ready to print
var $ean128; // flag for EAN128

// auxiliary 
var $i; 
var $len; 

########## CHARSET DEPENDANT INITIALIZATIONS

function CODE128() {

   parent::BarCode();
   
   $this->ean128 = FALSE;
   
   $this->bars_set = array (

   // official set of 103 symbols

      '212222','222122','222221','121223','121322','131222','122213','122312',
      '132212','221213','221312','231212','112232','122132','122231','113222',
      '123122','123221','223211','221132','221231','213212','223112','312131',
      '311222','321122','321221','312212','322112','322211','212123','212321',
      '232121','111323','131123','131321','112313','132113','132311','211313',
      '231113','231311','112133','112331','132131','113123','113321','133121',
      '313121','211331','231131','213113','213311','213131','311123','311321',
      '331121','312113','312311','332111','314111','221411','431111','111224',
      '111422','121124','121421','141122','141221','112214','112412','122114',
      '122411','142112','142211','241211','221114','413111','241112','134111',
      '111242','121142','121241','114212','124112','124211','411212','421112',
      '421211','212141','214121','412121','111143','111341','131141','114113',
      '114311','411113','411311','113141','114131','311141','411131',

   // plus 3 Start & 1 Stop characters (note stop has 7 modules)

      "211412", "211214", "211232", "2331112"
   );

}

################### PRE PROCESS

function preProcess() {

   $this->preProcess_1();
   if ( $this->charNotInSet ) return;
   $this->preProcess_2();
   if ( $this->errorDesc ) return;
   $this->preProcess_3();
}


########## PRE PROCESS PHASE 1 (VERIFY CHARSET)

function preProcess_1() {

   $this->len = strlen($this->code);
   $this->i = 0;

   //*** Preliminary generic control 

   for ( $i = 0; $i < $this->len; $i++ ) {

      // ASCII decimal value of char in $i position
      $c = ord( $this->code{$i} ); 

      if ( $c > 127 ) {
         $this->charNotInSet = "ASCII $c";
         return;
      }
   }

}

########## PRE PROCESS PHASE 2 ( OBTAIN HUMAN READABLE INTERPRETATION )

function preProcess_2() { 

   $this->codeText = $this->code;
}


########## PRE PROCESS PHASE 3 (ENCODE)

function preProcess_3() {

   $this->preProcess_3a();
   $this->preProcess_3b();

   // MAIN LOOP
   while ( $this->i < $this->len ) $this->oneDataLoop();

   $this->preProcess_3c();
}


########## PRE PROCESS PHASE 3 (ENCODE)

function preProcess_3a() {

   //*** Start Character 

   // " 1. Determine the Start character:
   // CASE 1a) If the data consist of 2 digits, use Start character C."

   if ( $this->len == 2 AND $this->isDigit(0) AND $this->isDigit(1) )
      $this->set = 'C';

   // CASE 1b) "If the data begins with 4 or more numeric data characters,
   // use Start character C;"

   elseif ( $this->len >= 4 AND $this->numberOfDigits() >= 4 ) 
      $this->set = 'C';

   // CASE 1c) "If an ASCII control character (e.g., NUL) occurs in the data 
   // before any lower case character, use Start character A."

   elseif ( $this->controlBeforeLower() ) 
      $this->set = 'A';

   // CASE 1d) "Otherwise, use Start character B."

   else
      $this->set = 'B';

 
   switch ( $this->set ) {
   case 'A': $this->encoded = START_A; break;
   case 'B': $this->encoded = START_B; break;
   default:  $this->encoded = START_C;
   }

}


function preProcess_3b() {

   /* CASE 1 (bis)
   "If Start character C is used and the data begins with an odd number
   of numeric data characters, insert a Code Set A or Code Set B character
   before the last digit, following rules 1c and 1d above to determine 
   between Code Sets A and B."
   */
      
   if ( $this->set == 'C' ) {
      $ndig = $this->numberOfDigits();
      if ( $ndig % 2 == 0 ) {
         $this->encodeDigits( $ndig  );

      } else {

         $this->encodeDigits( $ndig-1 );

         if ( $this->controlBeforeLower() ) {
            $this->set = 'A';
            $this->encoded .= CODE_A;
         } else {
            $this->set = 'B';
            $this->encoded .= CODE_B;
         }
         $this->encode_AB();
      }
   }
}


function preProcess_3c() {

   //*** Add Check Character

   if ( $this->checkCharacter ) { // should be always true

      $sum = ord($this->encoded{0}); // start char, weight 1
      $w = 1;

      // calculate weighted sum starting by position 1 and weight 1
      for ( $i = 1; $i < strlen($this->encoded); $i++) 
        $sum += ord($this->encoded{$i}) * $w++;

      // obtain check char as remainder of dividing by 103
      $this->encoded .= chr( $sum % 103 );
    }

   // END
   $this->encoded .= STOP;

}  // OF PREPROCESS()

##### IS CONTROL

function oneDataLoop() {

   // 2. If 4 or more numeric data characters occur together when in 
   //    Code Sets A or B and: ...

   if ( $this->set <> 'C' ) {   
      $ndig = $this->numberOfDigits();
      if ( $ndig  >= 4 ) {

         // 2a) If there is an even number of numeric data characters,
         // insert a Code Set C character before the first numeric digit 
         // to change to Code Set C;
         // 2b) If there is an odd number of numeric data characters,
         // insert a Code Set C character immediately after the first 
         // numeric digit to change to Code Set C.

         if ( $ndig % 2 == 1 ) {
            $this->encode_AB();
            $ndig--;
         }
         $this->set = 'C';
         $this->encoded .= CODE_C;
         $this->encodeDigits( $ndig );
         return; 
      }
   }

   // 3.When in Code Set B and an ASCII control character occurs in 
   // the data:...

   if ( $this->set == 'B' ) {   
      if ( $this->isControl( $this->i ) ) {

         // 3a) If following that character, a lower case character 
         // occurs in the data before the occurrence of another control
         // char., insert a Shift character before the control character;
         // b) Otherwise, insert a Code Set A character before the control 
         // character to change to Code Set A.

         if ( $this->lowerBeforeControl() ) 
            $this->encoded .= SHIFT;
         else {
            $this->set = 'A';
            $this->encoded .= CODE_A;
            }
      } 

      $this->encode_AB();
      return; 
   }

   // 4. When in Code Set A and a lower case character occurs in the data:	

   if ( $this->set == 'A' ) {   
      if ( $this->isLower( $this->i ) ) {

         // 4a) If following that character, a control character occurs 
         // in the data before the occurrence of another lower case character, 
         // insert a Shift character before the lower case character;
         // 4b) Otherwise, insert a Code Set B character before the lower 
         // case character to change to Code Set B.

         if ( $this->controlBeforeLower() ) 
            $this->encoded .= SHIFT;
         else {
            $this->set = 'B';
            $this->encoded .= CODE_B;
            }
      } 

      $this->encode_AB();
      return; 
   }

   // 5. When in Code Set C and a non-numeric character occurs in the data,
   // insert a Code Set A or Code Set B character before that character, 
   // following rules 1c and 1d to determine between Code Sets A and B.


   if ( $this->set == 'C' ) {

      // When we reach this point we should have a non-digit char
      // as we already encode all digits found in the previous cases
   
      // special case EAN128 field separator found
      // Then - encode it (FNC1)
      // - encode following digits if more than 4
      // - finish if end of code found
      // - continue loop in case new separator found
      // - continue with normal process otherwise,
      //   selecting A or B character set to encode following chars

      while ( $this->ean128 AND $this->code{$this->i} == SPACE )  {

         $this->encoded .= FNC1;
         $this->i++;

         $ndig = $this->numberOfDigits();

         if ( $ndig < 4 )
            break; // change char set

         else
            if ( $ndig % 2 == 0 ) { // even nr of digits

               $this->encodeDigits( $ndig  ); 
               if ( $this->i >= $this->len ) return;
               // continue as next char may be the separator
            } else {
               $this->encodeDigits( $ndig-1 );
               break; // change char set
            }

      } // of while


      // Normal process

      if ( $this->controlBeforeLower() ) {
         $this->set = 'A';
         $this->encoded .= CODE_A;
      } else {
         $this->set = 'B';
         $this->encoded .= CODE_B;
      }
      $this->encode_AB();
   }

} 


##### IS CONTROL

function isControl( $i ) {
   return ( ord( $this->code{$i} ) <= 31 );
}

##### IS UPPER

function isUpper( $i ) {
   return ( ord( $this->code{$i} ) >= 32 AND ord( $this->code{$i} ) <= 95 );
}

##### IS LOWER

function isLower( $i ) {
   return ( ord( $this->code{$i} ) >= 96 );
}

##### IS DIGIT

function isDigit( $i ) {

   $c = $this->code{$i};
   return ( $c >= '0' AND $c <= '9' );
}

###### NUMBER OF DIGITS

// Number of consecutive digits from current position 

function numberOfDigits() {

  $j = $this->i;
  while ( $j < $this->len AND $this->isDigit($j) ) $j++;
  return $j - $this->i;
}

##### CONTROL BEFORE LOWER

// 1c) If an ASCII control character (e.g., NUL) occurs in the data 
// before any lower case character...

function controlBeforeLower() {

   $i = $this->i + 1; // we look AFTER current position

   // Look for a control or lowercase character 
   while ( $i < $this->len AND $this->isUpper($i) ) $i++;

   // Not found
   if ( $i == $this->len ) return FALSE;

   return $this->isControl($i);
}

##### LOWER BEFORE CONTROL

function lowerBeforeControl() {

   $i = $this->i + 1; // we look AFTER current position

   // Look for a control or lowercase character 
   while ( $i < $this->len AND $this->isUpper($i) ) $i++;

   // Not found
   if ( $i == $this->len ) return FALSE;

   return $this->isLower($i);
}

#################### ENCODE AN EVEN NUMBER OF DIGITS

function encodeDigits( $n ) {

   $n = $n / 2;
   for ( $i = 1; $i <= $n; $i++ ) 
      $this->encode_C();
}

#################### ENCODE SET A OR B CHARACTER

function encode_AB() {

   // EAN128 field separator special case

   if ( $this->ean128 AND $this->code{$this->i} == SPACE ) {
      $this->encoded .= FNC1;
      $this->i++;
      return;
   }

   $i = ord($this->code{$this->i});
   if ( $i < 32 ) 
      $i += 64;  // control chars (set A)
   else
      $i -= 32;  // upper case chars (sets A&B) and lower case chars (set B)
   $this->encoded .= chr($i);
   $this->i++;
}

#################### ENCODE SET C CHARACTER ( TWO DIGITS IN ONE )

function encode_C() {

   $i = $this->i;
   $i = (int)( $this->code{$i} * 10 ) + (int)( $this->code{$i+1} );
   $this->encoded .= chr( $i ); 
   $this->i += 2;
}


#################### PAINT BODY (MAIN CODE)

function paintBody() {

   // paint chars in string 
   // ( Start & Stop characters already included in code )

   ### PAINT LOOP

   for ( $i = 0; $i < strlen($this->encoded); $i++ ) {
      $this->paintChar( $this->bars_set[ ord($this->encoded{$i}) ] );
      #$this->codeText .= "-".ord($this->encoded{$i});
   }

   return TRUE;
}


} // OF CLASS

?>