<?php

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.

require("BarCode.php");

class CODE11 extends BarCode {

######### CONSTRUCTOR

function CODE11() {

parent::BarCode();

if ( empty($this->base_set) ) 
   $this->base_set = array (
      '0' => 'nnnnw', '1' => 'wnnnw', '2' => 'nwnnw', '3' => 'wwnnn',
      '4' => 'nnwnw', '5' => 'wnwnn', '6' => 'nwwnn', '7' => 'nnnww',
      '8' => 'wnnwn', '9' => 'wnnnn', '-' => 'nnwnn');

$this->startChar = "nnwwn";
$this->stopChar  = "nnwwn";
$this->separatorBar= "n";
}

######### ADD CHECK CHARACTER

function addCheckChar() {

   $cod1 = $this->weightedCheckChar( 1, 0, 11, 11 );
   $this->code .= $this->set_keys[$cod1];

   // check character K, only if lentgh > 10

   if ( strlen($this->code) > 10) {
     $cod2 = $this->weightedCheckChar( 2, $cod1, 10, 11 );
     $this->code .= $this->set_keys[$cod2];
   }
}

}

?>