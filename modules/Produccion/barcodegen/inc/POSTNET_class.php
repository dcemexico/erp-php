<?php

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.

require("BarCode.php");

class POSTNET extends BarCode {

// height of POSTNET's tall & short bars.
var $heightTallBar;
var $heightShortBar;

// private vars for paintChar()
var $topShort;


######### CONSTRUCTOR

function POSTNET() {

   parent::BarCode();

   if ( empty($this->digitset) ) 
      $this->digitset = array (
         '11000','00011','00101','00110','01001',
         '01010','01100','10001','10010','10100' );

   $this->startChar = "1";
   $this->stopChar  = "1";
   $this->showCheckChar = FALSE;

   // defaults are set at init()
   $this->setHeightTallBar(0);
   $this->setHeightShortBar(0);
}

######### PARAMETERS

function setHeightTallBar( $n ) {
   $this->heightTallBar = $n;
}

function setHeightShortBar( $n ) {
   $this->heightShortBar = $n;
}

#########  INIT

function init() {

   parent::init(); 

   // use generic bar height parameter if there was no call
   // to specific function setHeightTallBar()
   // otherwise set a default value
   // set generic bar height to the tall bar size so generic
   // BarCode class methods works properly

   if ( $this->heightTallBar == 0 ) {
      if ( $this->barHeight > 0 ) 
         $this->heightTallBar = $this->barHeight;
      else
         $this->heightTallBar = 10; // default
   }
   $this->barHeight = $this->heightTallBar;

   // if not set, default short bar height to 50% of tall bar

   if ( $this->heightShortBar == 0 )
      $this->heightShortBar = (int) ( $this->heightTallBar / 2 ); 
   
   // calculate top margin for short bars

   $this->topShort = $this->topMargin 
      + $this->heightTallBar - $this->heightShortBar;
}


###### PAINT CHAR

function paintChar( $patternBars )  {

   for ( $i=0; $i < strlen($patternBars); $i++) {

      if ( $patternBars{$i} == '0' )
         $this->g->fillRect( $this->currentX, $this->topShort, 
            $this->narrowBarPixels, $this->heightShortBar );
      else
         $this->g->fillRect( $this->currentX, $this->topMargin, 
            $this->narrowBarPixels, $this->heightTallBar );

      $this->currentX += $this->narrowBarPixels + $this->wideBarPixels;

   } // of for

} // of paintChar()

###### ADD CHECK CHARACTER

function addCheckChar()  {

   $cc = $this->sum % 10;
   if ( $cc != 0 ) $cc = 10 - $cc;
   $this->code .= $cc;
}

} // OF CLASS

?>