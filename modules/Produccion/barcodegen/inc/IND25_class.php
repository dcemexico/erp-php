<?php

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.


require("SET25.php");

class IND25 extends SET25 {

######### CONSTRUCTOR

function IND25() {

   parent::SET25();
   $this->startChar = "wwwwnw";
   $this->stopChar  = "wwnww";
   $this->showCheckChar = FALSE;
}

###### PAINT LOOP

function paintLoop() {

   for ( $i = 0; $i < strlen($this->code); $i++ ) {

      // get paint pattern
      $patt = $this->digitset[ $this->code{$i} ];

      // draw bar of first char and space of second char, all 5 components per char
      for ( $j = 0; $j < 5; $j++ ) 
         $this->paintChar( $patt{$j} . "w" );
   }
}


} // OF CLASS


?>