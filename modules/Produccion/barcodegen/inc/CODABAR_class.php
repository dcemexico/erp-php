<?php

//  J4L BarCodes-1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.

require("BarCode.php");

class CODABAR extends BarCode {

var $CODABARStartChar;
var $CODABARStopChar;

######### CONSTRUCTOR

function CODABAR() {

   parent::BarCode();

   $this->barType = "CODABAR";

   if ( empty($this->base_set) ) 
   $this->base_set = array (
      '0' => 'nnnnnww','1' => 'nnnnwwn','2' => 'nnnwnnw','3' => 'wwnnnnn',
      '4' => 'nnwnnwn','5' => 'wnnnnwn','6' => 'nwnnnnw','7' => 'nwnnwnn',
      '8' => 'nwwnnnn','9' => 'wnnwnnn','-' => 'nnnwwnn','$' => 'nnwwnnn',
      ':' => 'wnnnwnw','/' => 'wnwnnnw','.' => 'wnwnwnn','+' => 'nnwnwnw',
      'A' => 'nnwwnwn','B' => 'nwnwnnw','C' => 'nnnwnww','D' => 'nnnwwwn' );

   $this->setStartChar('A');
   $this->setStopChar('B');
   $this->separatorBar= "n";
}

######### PARAMETERS

function setStartChar( $c ) {
  $this->CODABARStartChar = $c;
  $this->startChar = $this->base_set[$c];
}

function setStopChar( $c ) {
  $this->CODABARStopChar = $c;
  $this->stopChar = $this->base_set[$c];
}

########## ADD CHECK CHARACTER

function addCheckChar() {

   // Count also start & stop chars
   $this->sum += $this->set_indexes[ $this->CODABARStartChar ];
   $this->sum += $this->set_indexes[ $this->CODABARStopChar ];

   $cc = $this->sum % 16;
   if ( $cc != 0 ) $cc = 16 - $cc;

   $this->code .= $this->set_keys[$cc];
}

} // of class CODABAR

?>