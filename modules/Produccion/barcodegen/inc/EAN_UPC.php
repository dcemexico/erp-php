<?php 

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.

require("BarCode.php");

class EAN_UPC extends BarCode {

   // Guard Bars are longer flag
   var $longGuardBars;

   // User supplied supplement
   var $supplement;

   // Flags to be set to show 2 o 5 digits supplement
   var $supp2Flag;
   var $supp5Flag;

   /** Separation in pixels between the barcode and the supplement */
   var $suppSeparation;

   /** height of the supplement. 
       This is a multiplicator of code's height. 
       Default is 0.8 (80%). */
   var $suppHeight;

   var $suppStartChar;

   /** additional margin. Used for supplements. */
   var $barTopMargin;

   var $extraHeight; // used for guard bars


   // limits to print text
   var $leftTextStart;
   var $leftTextEnd;
   var $rightTextStart;
   var $rightTextEnd;
   var $endOfCode;

   var $setA;
   var $setB;
   var $setEANCode;

   var $fiveSupp;

   var $codeLength;

########## CONSTRUCTOR 

function EAN_UPC() {

   parent::Barcode();

   $this->startChar = "nnn";
   $this->stopChar = "nnn";
   $this->longGuardBars = TRUE;
   $this->setSupp2Flag( FALSE );
   $this->setSupp5Flag( FALSE );
   $this->setSuppSeparation( $this->leftMargin );
   $this->setSuppHeight( 0.8 );
   $this->suppStartChar = 2;

   $this->setA = array (
   '3211', '2221', '2122', '1411', '1132',
   '1231', '1114', '1312', '1213', '3112');

   $this->setB = array (
   '1123', '1222', '2212', '1141', '2311',
   '1321', '4111', '2131', '3121', '2113');

   $this->digitset = 0; // trick to activate digit validation
}


######### PARAMETERS

function setSupp2Flag( $s ) { $this->Supp2Flag = $s; }
function setSupp5Flag( $s ) { $this->Supp5Flag = $s; }

function setSupplement( $s ) {

   switch ( strlen($s) ) {
   case 2:
   case 5:
     $this->supplement = $s; 
   }
}

function setQuiteZone( $m ) {
   parent::setQuiteZone( $m );
   $this->setSuppSeparation( $m );
}

function setSuppSeparation( $s ) { 

   if ( $s >= 1 ) $this->suppSeparation = (int) $s;
}

function setSuppHeight( $h ) { $this->suppHeight = $h; }


#################### PAINT WITH SUPPLEMENT

function paintWithSupp( $code, $supp, $filename='' ) {

   $this->setSupplement( $supp );
   return parent::paint( $code, $filename );
}

#################### PAINT 

function paint( $code, $filename='' ) {

   $a = explode( "-", $code );
   if ( count($a) == 2 ) 
      return $this->paintWithSupp( $a[0], $a[1], $filename );
   else
      return BarCode::paint( $code, $filename );
}


################### PAINT FINISH

function paintFinish() {

   $this->supplement = '';
}

#################### PAINT BODY

function paintBody() {

   $this->odd3mod10CheckChar();

   if ( strlen($this->code) <> $this->codeLength ) {
      $this->errorDesc = "Invalid code length ".strlen($this->code);
      return FALSE;
   }

   $this->codeText = $this->code;

   ########## DRAW BARS

   // paint start character
   $this->paintGuardChar( $this->startChar );
   $this->leftTextStart = $this->currentX;

   // paint chars in string (check char. included)
   $this->paintLoop();
   $this->rightTextEnd = $this->currentX;

   // paint stop char
   $this->paintGuardChar( $this->stopChar );
   $this->endOfCode = $this->currentX;

   // supplement
   $this->paintSupplement();

   return TRUE;
}

#################### SIMPLE DEFAULT PAINT LOOP (valid for EAN8 and UPCA)

function paintLoop() {

   // 4 for EAN8, 6 for UPCA
   $middle_char = $this->codeLength / 2;

   // first group

   $this->whiteFirst = TRUE;
   for ( $i = 0; $i < $middle_char ; $i++ ) 
      $this->paintChar( $this->setA[ $this->code{$i} ] );

   // center guard Pattern

   $this->leftTextEnd = $this->currentX;
   $this->whiteFirst = TRUE;
   $this->paintGuardChar("nnnnn");
   $this->rightTextStart = $this->currentX;

   // second group

   $this->whiteFirst = FALSE;
   for ( $i = $middle_char; $i < $this->codeLength ; $i++ ) 
      $this->paintChar( $this->setA[ $this->code{$i} ] );
}


##########   PAINT SUPPLEMENT

function paintSupplement() {

   $count = strlen( $this->supplement );

   if ( $count > 0 )
      $supp = $this->supplement;
   else {

      if ( $this->Supp2Flag ) $count = 2; 
      else if ( $this->Supp5Flag ) $count = 5;

      if ( $count == 0 ) return; // no supplement
      
      $supp = substr( $this->code, $this->suppStartChar, $count );
   }

   $this->barTopMargin = (int) ( $this->barHeight * ( 1 - $this->suppHeight ) );

   $this->currentX += $this->suppSeparation;

   $start = $this->currentX;

   if ( $count == 2 ) {
      $setSupp = array ("AA","AB","BA","BB");
      $parity = $setSupp[ $supp % 4 ];

   } else {
      $setSupp = array 
         ("BBAAA","BABAA","BAABA","BAAAB","ABBAA",
          "AABBA","AAABB","ABABA","ABAAB","AABAB");

      // To determine the parity pattern for the five digit supplementals, use the following steps:
      //
      // Designate the rightmost character odd
      // Sum all of the characters in the odd positions and multiply the result by three.
      // Sum all of the characters in the even positions and multiply the result by nine.
      // Add the results from steps two and three. The number in the units position is the number of the parity pattern.
      
      $len = strlen($supp);
      $odd = TRUE;
      $sumodd = 0;
      $sum = 0;
   
      for ( $i = $len-1; $i >= 0; $i-- ) {
        if ($odd) $sumodd += $supp{$i};
        else      $sum    += $supp{$i};
        $odd = (!$odd);
      }
   
      $sum = (string) ($sumodd * 3 + $sum * 9);
   
      // take last digit
   
      $parity = $setSupp[ $sum{ strlen($sum)-1 } ];
   }

   // start
   $this->paintGuardChar("112");

   $this->whiteFirst = TRUE;

   $count--;

   for ( $j = 0; $j <= $count; $j++ ) {
      $this->paintSuppChar( $parity{$j}, $supp{$j} );
      if ( $j < $count ) 
        $this->paintGuardChar("11" ); // delineator
   }

   $this->whiteFirst = FALSE;

   $end = $this->currentX;

   // Draw supplement text

   $this->drawSuppText( $supp, $start, $end );

   $this->barTopMargin = 0;
}

#################### DRAW TEXT

function drawSplitText( $first_char, $left_group, $right_group, $last_char="") {

   // If not long guard bars use default draw text function
   
   if ( ! $this->longGuardBars ) {
     parent::drawText();
     return;
   }
   
   $this->g->setColor( $this->fontColorId );

   // Following code only for text inside guardbars
   
   $TextH = $this->g->getFontHeight();
   $y = $this->barHeight + $TextH + 1 + $this->topMargin;
   
   // first flag (printed in the "quiet zone")
   
   if ( strlen($first_char) > 0 )
      $this->g->drawString( $first_char,
         $this->leftMargin - $this->g->stringWidth($first_char) - 2,
         $y );
   
   // Draw left group text 
   
   $center = (int) ( $this->leftTextEnd - $this->leftTextStart 
      - $this->g->stringWidth($left_group)) / 2;
   if ( $center < 0 ) $center = 0;
   $this->g->drawString( $left_group, $this->leftTextStart + $center, $y );
   
   // Draw right group text 
   
   $center = (int) ( $this->rightTextEnd - $this->rightTextStart 
      - $this->g->stringWidth($right_group)) / 2;
   if ( $center < 0 ) $center = 0;
   $this->g->drawString( $right_group, $this->rightTextStart + $center, $y );
   
   // last flag (printed in the "quiet zone")
   
   if ( strlen($last_char) > 0 )
      $this->g->drawString( $last_char, $this->endOfCode + 3, $y );

   // update currentY. Important for final image reduction.
   $this->currentY = $y;

}

##########  DRAW SUPPLEMENT TEXT 

function drawSuppText( $supp, $start, $end ){

   $y = $this->topMargin + $this->barTopMargin;
   $center = (int) (( $end - $start - $this->g->stringWidth($supp) ) / 2 );
   if ( $center < 0 ) $center = 0;
   $this->g->setColor( $this->fontColorId );
   $this->g->drawString( $supp, $start + $center, $y );
}

######### PAINT GUARD CHAR

function paintGuardChar( $patternBars )  {

   // Guard bars are longer that the other

   $this->extraHeight = $this->g->getFontHeight();
   $this->paintChar( $patternBars );
   $this->extraHeight=0;
}


##########  PAINT SUPPLEMENT CHAR

function paintSuppChar( $set, $char )  {

   if ( $set == "A" ) 
     $this->paintGuardChar( $this->setA[ $char ] );
   else
     $this->paintGuardChar( $this->setB[ $char ] );
}

##########  PAINT INVERTED CHAR

function paint_inverted_char( $patternBars )  {

   $this->whiteFirst = TRUE;
   $this->paintChar( $patternBars );
   $this->whiteFirst = FALSE;
}

##########  ADD BAR

function addBar($w,$black) {

   if ($black) {
      $this->g->fillRect( 
         $this->currentX, 
         $this->topMargin + $this->barTopMargin,
         $w, 
         $this->barHeight + $this->extraHeight - $this->barTopMargin );
   }

   // move pointer
   $this->currentX += $w;
}

} // OF CLASS
?>