<?php

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.

require("BarCode.php");

class SET25 extends BarCode {

######### INITIALIZATIONS

function SET25() {

   parent::BarCode();

   if ( empty($this->digitset) ) 
      $this->digitset = array (
      "nnwwn","wnnnw","nwnnw","wwnnn","nnwnw",
      "wnwnn","nwwnn","nnnww","wnnwn","nwnwn" );
}


############ CHECK CHAR

function addCheckChar() { $this->odd3mod10CheckChar(); }


} // OF CLASS


?>