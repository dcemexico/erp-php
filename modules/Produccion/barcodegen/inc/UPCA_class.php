<?php

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.

require("EAN_UPC.php");

class UPCA extends EAN_UPC {

function UPCA() {

   parent::EAN_UPC();
   $this->codeLength = 12;
   $this->suppStartChar = 1;
}

function drawText() {

  $this->drawSplitText(
      $this->codeText{0},
      substr($this->codeText,1,5), 
      substr($this->codeText,6,5),
      $this->codeText{11} );
}

} // OF CLASS

?>