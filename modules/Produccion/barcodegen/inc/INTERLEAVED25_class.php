<?php

//  J4L BarCodes for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.

require("SET25.php");

class INTERLEAVED25 extends SET25 {

######### CONSTRUCTOR

function INTERLEAVED25() {

   parent::SET25();
   $this->startChar = "nnnn";
   $this->stopChar  = "wnn";
}

###### PAINT LOOP

function paintLoop() {

   // If odd nr of chars, add leading "0"
   if ( strlen($this->code) % 2 == 1 ) $this->code = "0".$this->code;

   $this->codeText = $this->code;

   for ( $i = 0; $i < strlen($this->code); $i+=2 ) {

      // get paint patterns for two chars
      $patt1 = $this->digitset[ $this->code{$i} ];
      $patt2 = $this->digitset[ $this->code{$i+1} ];

      // draw bar of first char and space of second char, all 5 components per char
      for ( $j = 0; $j < 5; $j++ ) 
         $this->paintChar( $patt1{$j} . $patt2{$j} );
   }
}

} // OF CLASS


?>