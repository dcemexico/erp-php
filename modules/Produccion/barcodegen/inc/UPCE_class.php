<?php

//  J4L BarCodes 1D for PHP
//  Copyright (C) Java4Less.com
//  All rights reserved
//
// Adquisition , use and distribution of this code is subject to restriction:
//  - You may modify the source code in order to adapt it to your needs.
//  - Redistribution of this (or a modified version) source code is prohibited.
//  - You may not remove this notice from the source code.
//  - This notice disclaim all warranties of all material.
//  - You may not copy and paste any code into external files.
//  - Use of this software on more than one server
//    requires the appropriate license.


require("EAN_UPC.php");

class UPCE extends EAN_UPC {

var $UPCESystem;

############ CONSTRUCTOR

function UPCE() {

   parent::EAN_UPC();
   $this->stopChar = "nnnnnn";
   $this->codeLength = 12;
   $this->suppStartChar = 0;
   $this->UPCESystem = 1;
}

############ SET SYSTEM

function setSystem( $s ) { $this->UPCESystem = $s; }

############ PAINT LOOP

function paintLoop() {

   $c = $this->code;

   // case one & two

   if ( $c[4] == "0" && $c[5] == "0" ) {

      if ( $c[3] <= 2 )
         $code6 = $c[1].$c[2].$c[8].$c[9].$c[10].$c[3];
      else
         $code6 = $c[1].$c[2].$c[3].$c[9].$c[10]."3";

   // case three & four

   } else {
      if ( $this->code[5] == "0" ) 
         $code6 = $c[1].$c[2].$c[3].$c[4].$c[10]."4";
      else
         $code6 = $c[1].$c[2].$c[3].$c[4].$c[5].$c[10];
   }

   $this->codeText = $code6;

   if ( $this->UPCESystem == 0 ) 
      switch ( $c[11] ) { // Check character
      case "0": $sys = "BBBAAA"; break;
      case "1": $sys = "BBABAA"; break;
      case "2": $sys = "BBAABA"; break;
      case "3": $sys = "BBAAAB"; break;
      case "4": $sys = "BABBAA"; break;
      case "5": $sys = "BAABBA"; break;
      case "6": $sys = "BAAABB"; break;
      case "7": $sys = "BABABA"; break;
      case "8": $sys = "BABAAB"; break;
      case "9": $sys = "BAABAB"; break;
      }
   else
      switch ( $c[11] ) {
      case "0": $sys = "AAABBB"; break;
      case "1": $sys = "AABABB"; break;
      case "2": $sys = "AABBAB"; break;
      case "3": $sys = "AABBBA"; break;
      case "4": $sys = "ABAABB"; break;
      case "5": $sys = "ABBAAB"; break;
      case "6": $sys = "ABBBAA"; break;
      case "7": $sys = "ABABAB"; break;
      case "8": $sys = "ABABBA"; break;
      case "9": $sys = "ABBABA"; break;
      }

   // paint char in string

   $this->whiteFirst = TRUE;

   for ( $i = 0; $i < 6 ; $i++ ) {
      if ( $sys[$i]  == "A" )
         $this->paintChar( $this->setA[ $code6{$i} ] );
      else
         $this->paintChar( $this->setB[ $code6{$i} ] );
   }

  $this->leftTextEnd = $this->currentX;

}

############ DRAW TEXT

function drawText() {

  $this->drawSplitText( $this->code{0}, $this->codeText, "", "" );
}


} // OF CLASS

?>