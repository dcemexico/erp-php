<!DOCTYPE>
<html>
<head>
	<title>Production</title>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../../vendor/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../../vendor/fixedColumns.dataTables.css">
	<link rel="stylesheet" href="../../assets/css/panel.css">
	<link rel="stylesheet" href="../../assets/css/tema_tabla.css">
	<!-- <link rel="stylesheet" href="../../assets/css/item.css"> -->
</head>
<body>

	<?php
		include "../header_menu.php";
		include "../../database/conexion.php";
	?>

	<div class="container">
		<h2>Production</h2>
		<hr />
		<div class="row">
			<div class="col-md-2">
				<a id="show_new_item" class="btn btn-success"> <i class="fa fa-plus"></i> Generar label</a>
			</div>
			<div class="col-md-2">
				<a id="show_new_item" class="btn btn-info" href="productionscann.php" onClick="javascript:window.open('','fenster','width=1350,height=670,scrollbars=no')" target="fenster"> <i class="fa fa-spinner"></i> Scann Label</a>
			</div>
		</div>
<hr />
		<form id="content_new_item" class="hide_content well" onsubmit="javascript:window.open('','fenster','width=300,height=150,scrollbars=yes')" action="labelGenerate.php" method="post" target="fenster">
			<div class="row">
				<div class="col-md-6">
					<label>Material:</label>
					<select name="materialf" class="form-control" required>
					<option value="">--</option>
					 <?php 
			              $consulta = "SELECT item_material FROM item where item_material != ' ' group by item_material ";
					        $resultado = $mysqli -> query($consulta);
					        while($fila = $resultado -> fetch_array())
					        {
					         $material = $fila["item_material"];
					         echo "<option value='$material'>$material</option>";
					        }
			              ?>
					</select>
				</div>
				<div class="col-md-6">
					<label>Cantidad:</label><br>
					<input type="text" class="form-control" name="cantidad" required>
				</div>
				<div class="col-md-6">
					<label>Linea:</label>
					<select name="linea" class="form-control" required>
						<option value="">--</option>
						<option value="LineA">LineA</option>
						<option value="LineB">LineB</option>
						<option value="LineC">LineC</option>
						<option value="LineD">LineD</option>
					</select>
				</div>
				<div class="col-md-6">
					<label>Turno:</label>
					<select class="form-control" name="turno" required>
						<option value="">--</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
					</select>
				</div>
				<div class="col-md-6">
					<br />
					<button type="submit" class="btn btn-success" >Guardar</button>
				</div>
			</div>
		</form>


		<?php
		$Width_no = "40px";
		$Width_mat = "100px";
		$Width_desc = "250px";
		$Width_mtype = "150px";
		$Width_unit = "60px";
		$Width_loc = "150px";
		$Width_status = "150px";
		$Width_date = "150px";
		?>
		<div class="contenedor">
			<div  class="jqxgrid" id="dvData">
	      <table id="example" class="" >
					<thead>
						<tr>
							<th width="<?=$Width_no?>">#</th>
							<th width="<?=$Width_mat?>">Material</th>
							<th width="<?=$Width_desc?>">Descripcion</th>
							<th width="<?=$Width_mtype?>">Cantidad</th>
							<th width="<?=$Width_unit?>">Linea</th>
							<th width="<?=$Width_date?>">Fecha</th>
						</tr>
					</thead>
					<tbody>
				<?php
					$fecha_actual = date("Y-m-d");
					$query = "SELECT material,descripcion,linea,fecha,sum(cantidad) as total FROM pruductionscann where  fecha = '$fecha_actual' group by material ";
					$result = $mysqli -> query($query);
					$cont = 1;

					while($reg = $result -> fetch_array()) {
						$material_search = $reg['material'];
						$descripcion_search = $reg['descripcion'];
						$cantidad_search = $reg['total'];
						$linea_search = $reg['linea'];
						$fecha_search = $reg['fecha'];
						?>
						<tr>
							<td><?=$cont?></td>
							<td><?=$material_search?></td>
							<td><?=$descripcion_search?></td>
							<td><?=$cantidad_search?></td>
							<td><?=$linea_search?></td>
							<td><?=$fecha_search?></td>
						</tr>
						<?php
						$cont++;
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

<script type="text/javascript" src="../../vendor/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="../../vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../vendor/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="../../vendor/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../vendor/dataTables.jqueryui.js"></script>
<script type="text/javascript" src="../../vendor/dataTables.fixedColumns.js"></script>

<script type="text/javascript" language="javascript" src="../../assets/js/item.js"></script>
</body>
</html>
