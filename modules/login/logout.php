<?php
session_start();
if(!empty($_SESSION['authenticated'])) {
	include("db.php");
	$_SESSION['authenticated']='';
	session_destroy();
}
header("Location: ../../index.php");
?>
