<?php
include("../../database/db.php");
error_reporting(0);
session_start();

/* COMENTARIOS
 Con JSON es mas facil y rapido el envio de informacion.
 Se cambio el user_name por authenticated para que siga un lineamiento de lo que contiene ese dato y predecir lo que hace y para que va funcionar
*/

if(isSet($_POST['username']) && isSet($_POST['password'])) {

	$username = mysqli_real_escape_string($db,$_POST['username']);
	$password = (mysqli_real_escape_string($db,$_POST['password']));
	$result = mysqli_query($db,"SELECT * FROM users WHERE username='$username' and userpassword='$password'");
	$count = mysqli_num_rows($result);
	$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
	$response = []; // Se crea un arreglo que seria como respuesta - sea texto o sea listado de registros

	if($count==1) {
		$_SESSION['authenticated'] = $row['username'];
		$response['success'] = true;
		$response['redirect'] = 'modules/home/home.php';
	} else {
		$response['success'] = false;
		$response['message'] = 'Usuario y/o contraseña incorrectos.';
	}
	echo json_encode($response); //Convertir a JSON
	exit; // Finaliza
} else {
	$response['success'] = false;
	$response['message'] = 'Valores no enviados.';
}
echo json_encode($response); // Convertir a JSON
exit; // Finaliza
?>
