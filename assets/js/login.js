function signIn() {

  var username = document.getElementById('username').value;
  var password = document.getElementById('password').value;

  if($.trim(username).length && $.trim(password).length) {
    $.ajax({
      url: "/erp-php/modules/login/login.php",
      type: "POST",
      data: `username=${username}&password=${password}`,
      success: function(res) {
        var response = $.parseJSON(res);
        var errors = $('#errors');

        if (response.success) {
          window.location = response.redirect;
        }
        else {
          $(errors).html(response.message)
        }

      },
      error: function (err) {
      },
      complete: function (res) {
      }

    });
  }

}
