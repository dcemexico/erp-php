$('#show_new_item').on('click', function () {
  $('#content_new_item').toggle()
})

$('#show_edit_item').on('click', function () {
  $('#content_edit_item').toggle()
})

$('#show_new_line').on('click', function () {
  $('#content_new_Line').toggle()
})

$(document).ready(function() {
  var table = $('#example').DataTable( {
    scrollY:        "270px",
    scrollX:        true,
    scrollCollapse: false,
    sort: false,
    paging:         false,
    fixedColumns:   {
      leftColumns: 0,
      rightColumns: 0
    }
  });
});

$(document).ready(function() {
  var table = $('#table1').DataTable( {
    scrollY:        "270px",
    scrollX:        true,
    scrollCollapse: false,
    sort: false,
    paging:         false,
    fixedColumns:   {
      leftColumns: 0,
      rightColumns: 0
    }
  });
});

  $(document).ready(function(){
    $( "tbody" ).sortable();
});


  $( "tbody" ).sortable({
    // Cancel the drag when selecting contenteditable items, buttons, or input boxes
    cancel: ":input,button,[contenteditable]",
    // Set it so rows can only be moved vertically
    axis: "y",
    // Triggered when the user has finished moving a row
    update: function (event, ui) {
        // sortable() - Creates an array of the elements based on the element's id. 
        // The element id must be a word separated by a hyphen, underscore, or equal sign. For example, <tr id='item-1'>
        var data = $(this).sortable('serialize');
        
        //alert(data); <- Uncomment this to see what data will be sent to the server
 
        // AJAX POST to server
        $.ajax({
            data: data,
            type: 'POST',
            url: 'testsavesort.php',
            success: function(response) {
          // alert(response); //<- Uncomment this to see the server's response
      }
        });
    }
});